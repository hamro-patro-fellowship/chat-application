package com.hamropatro.model;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
public class Room {
    String roomId;
    String senderId;
    String receiverId;
}