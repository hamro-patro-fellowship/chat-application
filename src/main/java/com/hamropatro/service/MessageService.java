package com.hamropatro.service;

import com.hamropatro.model.Message;
import com.hamropatro.repo.MessageRepository;
import jakarta.inject.Singleton;

import java.util.List;

@Singleton
public class MessageService {

    private final MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<Message> getMessage(String roomId) {
        return messageRepository.getMessage(roomId);
    }
}
