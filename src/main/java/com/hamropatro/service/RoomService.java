package com.hamropatro.service;

import com.hamropatro.model.Room;
import com.hamropatro.repo.RoomRepository;
import jakarta.inject.Singleton;

@Singleton
public class RoomService {

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public String getRoomId(String receiverId, String senderId) {
        Room room = roomRepository.getRoom(receiverId, senderId);
        return room.getRoomId();
    }
}
