package com.hamropatro.service;

import com.hamropatro.model.User;
import com.hamropatro.repo.UserRepository;
import jakarta.inject.Singleton;

import java.util.ArrayList;


@Singleton
public class UsersService {
    private final UserRepository userRepository;

    public UsersService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public boolean getJoinResponse(String userName) {
        return !userRepository.exists(userName);
    }

    public boolean getLoginResponse(String userId, String userName) {
        User user = new User(userId, userName);
        return userRepository.existsUser(user);
    }

    public String addUser(String userName) {
        return userRepository.addUser(userName);
    }

    public boolean updateUser(String userId, String newUserName) {
        return userRepository.UpdateUser(userId, newUserName);
    }

    public boolean deleteUser(String userId) {
        return userRepository.DeleteUser(userId);
    }

    public ArrayList<User> getAllUserExcept(String myId) {
        return userRepository.getAllUserExcept(myId);
    }

    public ArrayList<User> getAllUser() {
        return userRepository.getAllUser();
    }
}
