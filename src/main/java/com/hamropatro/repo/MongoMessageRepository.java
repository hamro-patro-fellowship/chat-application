package com.hamropatro.repo;

import com.hamropatro.model.Message;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import jakarta.inject.Singleton;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class MongoMessageRepository implements MessageRepository{

    private final MongoClient db;

    public MongoMessageRepository(MongoClient db) {
        this.db = db;
    }

    private MongoDatabase getDb() {
        return db.getDatabase("chatDB");
    }

    public MongoCollection<Message> getCollection() {
        return getDb().getCollection("messageCollection", Message.class);
    }

    @Override
    public List<Message> getMessage(String roomId) {
        Bson filter = Filters.eq("_id", roomId);
        return getCollection().find(filter, Message.class).into(new ArrayList<>());
    }
}

