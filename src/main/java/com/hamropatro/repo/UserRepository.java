package com.hamropatro.repo;

import com.hamropatro.model.User;

import java.util.ArrayList;

public interface UserRepository {
    User getUser(String userId);
    String addUser(String userName);
    boolean exists(String userName);
    boolean existsUser(User user);
    ArrayList<User> getAllUser();
    boolean UpdateUser(String userId, String newUserName);
    boolean DeleteUser(String userId);
    ArrayList<User> getAllUserExcept(String myId);
}
