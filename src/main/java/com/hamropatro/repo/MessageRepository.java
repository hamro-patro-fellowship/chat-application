package com.hamropatro.repo;

import com.hamropatro.model.Message;

import java.util.List;

public interface MessageRepository {
    List<Message> getMessage(String roomId);
    }

