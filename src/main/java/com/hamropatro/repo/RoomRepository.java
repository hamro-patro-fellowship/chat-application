package com.hamropatro.repo;


import com.hamropatro.model.Room;

public interface RoomRepository {
    Room getRoom(String senderId, String receiverId);
}
