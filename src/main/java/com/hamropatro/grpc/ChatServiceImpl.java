package com.hamropatro.grpc;

import com.hamropatro.*;
import com.hamropatro.model.Message;
import com.hamropatro.model.User;
import com.hamropatro.service.AdminService;
import com.hamropatro.service.MessageService;
import com.hamropatro.service.RoomService;
import com.hamropatro.service.UsersService;
import io.grpc.stub.StreamObserver;
import io.micronaut.grpc.annotation.GrpcService;
import jakarta.inject.Singleton;

import java.util.ArrayList;
import java.util.List;


@GrpcService
@Singleton
public class ChatServiceImpl extends ChatServiceGrpc.ChatServiceImplBase {

    private final AdminService adminService;
    private final UsersService usersService;
    private final RoomService roomService;
    private final MessageService messageService;

    public ChatServiceImpl(AdminService adminService, UsersService usersService, RoomService roomService, MessageService messageService) {
        this.adminService = adminService;
        this.usersService = usersService;
        this.roomService = roomService;
        this.messageService = messageService;
    }

    @Override
    public void adminLogin(AdminLoginRequest request, StreamObserver<AdminLoginResponse> responseObserver) {
        String adminUserId = request.getAdminUserId();
        String adminUserName = request.getAdminUserName();
        boolean adminLogin = adminService.getLoginResponse(adminUserId, adminUserName);
        ArrayList<User> allUserList = usersService.getAllUser();
        ArrayList<FetchUsers> resultList = new ArrayList<>();

        AdminLoginResponse.Builder response = AdminLoginResponse.newBuilder();
        if(adminLogin) {
            allUserList.forEach(user -> {
                FetchUsers fu = FetchUsers.newBuilder()
                        .setUserId(user.getUserId())
                        .setUserName(user.getUserName())
                        .build();
                resultList.add(fu);
            });
        }
        response.addAllAdminUserList(resultList);
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void userLogin(UserLoginRequest request, StreamObserver<UserLoginResponse> responseObserver) {
        String userId = request.getUserId();
        String userName = request.getUserName();

        UserLoginResponse.Builder response = UserLoginResponse.newBuilder();

        boolean loginResponse = usersService.getLoginResponse(userId, userName);
        if (loginResponse) {
            response.setUserId(userId).setUserName(userName);
        }
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void join(JoinUserRequest request, StreamObserver<JoinUserResponse> responseObserver) {
        // this function can be only used by admin after admin is logged in.
        // when an admin decides to add user, he only gives username to add, by giving a username
        // we want to save that username in database and return the id from database.
        String userName = request.getUserName();
        String userId = usersService.addUser(userName);

        responseObserver.onNext(JoinUserResponse.newBuilder()
                .setUserId(userId)
                .setUserName(userName)
                .build());
        responseObserver.onCompleted();
    }

    @Override
    public void updateUser(UpdateUserRequest request, StreamObserver<UpdateUserResponse> responseObserver) {
        String userId = request.getUserId();
        String newUserName = request.getNewUserName();
        UpdateUserResponse.Builder response= UpdateUserResponse.newBuilder();
        boolean updateStatus = usersService.updateUser(userId, newUserName);
        if(updateStatus) {
            response.setUserId(userId).setUpdatedUserName(newUserName);
        }
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void deleteUser(DeleteUserRequest request, StreamObserver<DeleteUserResponse> responseObserver) {
        String userId = request.getUserId();
        DeleteUserResponse.Builder response = DeleteUserResponse.newBuilder();
        boolean deleteStatus = usersService.deleteUser(userId);
        if (deleteStatus){
            response.setSuccess(true);
        }
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getRoomId(GetRoomIdRequest request, StreamObserver<GetRoomIdResponse> responseObserver) {
        String senderId = request.getSenderId();
        String receiverId = request.getReceiverId();
        String roomId = roomService.getRoomId(receiverId, senderId);
        GetRoomIdResponse.Builder response = GetRoomIdResponse.newBuilder().setRoomId(roomId);
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void fetchMsg(FetchMsgRequest request, StreamObserver<FetchMsgResponse> responseObserver) {
        String roomId = request.getRoomId();
        List<Message> myMessage = messageService.getMessage(roomId);
        FetchMsgResponse.Builder response = FetchMsgResponse.newBuilder();
        List<FetchedMessage> output = new ArrayList<>();
        myMessage.forEach(message -> {
            FetchedMessage fm = FetchedMessage.newBuilder()
                    .setMessageReceived(message.getMessageReceived())
                    .setMessageSentTime(message.getMessageSentTime())
                    .setSentBy(message.getSentBy())
                    .build();
            output.add(fm);
        });

        response.addAllFetchedMessage(output);

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getAllUsers(GetAllUserRequest request, StreamObserver<FetchUserList> responseObserver) {
        String myId = request.getMyId();
        // myId is provided so that the list is returned except myId. On that I can choose other user to chat with
        // suppose I am A and I want to chat with B, C etc. what I need is list of B, C etc. but not A on the list.
        ArrayList<User> userList = usersService.getAllUserExcept(myId);
        ArrayList<FetchUsers> output = new ArrayList<>();
        userList.forEach(user -> { FetchUsers ul = FetchUsers.newBuilder()
                .setUserId(user.getUserId())
                .setUserName(user.getUserName())
                .build();
            output.add(ul);
        });

        FetchUserList.Builder response = FetchUserList.newBuilder();
        response.addAllUserList(output).build();


        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}
