syntax = "proto3";

option java_multiple_files = true;
option java_package = "com.hamropatro";
option java_outer_classname = "ChatApplication";
option objc_class_prefix = "HLW";

package com.example;

service ChatService {
  rpc adminLogin(AdminLoginRequest) returns (AdminLoginResponse) {} //at first admin should login as to add user, modify them or delete them.
  rpc userLogin(UserLoginRequest) returns (UserLoginResponse) {} //user login
  rpc join(JoinUserRequest) returns (JoinUserResponse) {} //this is performed by admin to add user
  rpc updateUser(UpdateUserRequest) returns (UpdateUserResponse) {} //this is performed by admin to update user
  rpc deleteUser(DeleteUserRequest) returns (DeleteUserResponse) {} //this is performed by admin to delete user
  rpc getRoomId(GetRoomIdRequest) returns (GetRoomIdResponse); // this is performed when sender first clicks on any friend's list
  rpc fetchMsg(FetchMsgRequest) returns (FetchMsgResponse) {} // to receive a message,we need to provide a user of which a message should be displayed. Here we display message
  rpc getAllUsers(GetAllUserRequest) returns (FetchUserList) {} //This is a list of user displayed once, login is done.
}

//we need provide admin userName and admin userId to login.
message AdminLoginRequest{
  string admin_user_id = 1;
  string admin_user_name = 2;
}

// After admin is logged in, we have to show userList on which the actions like Update and Delete is performed.
// This message is responsible to show the all list of users with user_id and user_name.
//So, message: FetchUsers contains user_name and user_id.
message  AdminLoginResponse{
  repeated FetchUsers admin_user_list = 2;
}

// this is used twice-
// 1. AdminLoginResponse - to list all the user in admin page
// 2. FetchUserList - to list all the user except the user who is logged in (to show friend list).
//    as My Username should not be shown in my list
message FetchUsers{
  string user_id = 1;
  string user_name = 2;
}

//provide credentials for user to login
message UserLoginRequest{
  string user_id = 1;
  string user_name = 2;
}

// we have to return the credentials of user after login is successful.
//credentials is returned this time as we don't other things to return.
// we may want to return profile image, different information of the logged in user in future
// for now we return user_id and user_name.
// the user name is used to show the user_name in profile, while user_id is useful when we want to fetch some other's message.
message  UserLoginResponse{
  string user_id = 1;
  string user_name = 2;
}

// this is used to add user which can be performed only by admin
// we provide only user_name to add, while user_id is returned in response after username is inserted.
message JoinUserRequest {
  string user_name = 2;
}

//after new user is inserted, user_id and user_name is returned.
// please make a message in frontend like User : "UserName" is added with user_id: "user_id".
//also perform getAllUser() in order to show the updated user list as this one is inserted.
//refresh should be done while updating and deleting as well.
message JoinUserResponse {
  string user_id = 2;
  string user_name = 3;
}

//This will return an array of user. Here, FetchUsers is an object of one user.
// Putting keyword repeated, it will return an array of similar object.
message FetchUserList {
  repeated FetchUsers user_list = 1;
}

message UpdateUserRequest{
  string user_id = 1;
  string new_user_name = 2;
}

message UpdateUserResponse {
  string user_id = 2;
  string updated_user_name = 1;
}

message DeleteUserRequest{
  string user_id = 1;
}

message DeleteUserResponse{
  bool success = 1;
}

//when A click in the B of friend list, sender_id will be A and receiver will be B. so, by getting sender_id and receiver_id
// the room_id is returned, which will be further used to do many things like fetching chat and websocket-chat in the beginning as well.
message GetRoomIdRequest{
  string sender_id = 1;
  string receiver_id = 2;
}

//will just return room_id
message GetRoomIdResponse{
  string room_id = 1;
}

// when user A is logged in, the user_list should be shown except user A. so, user A's id is passed so that user A will be filtered out
// from the list.
message GetAllUserRequest {
  string my_id = 1;
}

// the room_id that was requested before will be used to request the message in that chatroom.
message FetchMsgRequest {
  string room_id = 1;
}

// this will return an array of object FetchedMessage. The request above by giving room_id
// will be used to filter the message  of that room_id only.
// now the array of message of requested room_id is fetched.
// Each message object will contain sent_by, message_received and message_sent_time.
// While an array of same message object helps to fetch all the message in the provided room_id.
message FetchMsgResponse {
  repeated FetchedMessage fetchedMessage = 1;
}

// this is a single message object of specified room_id.
message FetchedMessage {
  string sent_by = 1;
  string message_received = 2;
  string message_sent_time = 3;
}
